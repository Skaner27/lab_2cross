<?php
@session_start();
class Client extends  User
{
    public function clientGreeting()
    {
        if ($_SESSION['NowLang'] == "ru") {
            echo "Здравствуйте, Клиент " . $this->name . " " . $this->surname . ". Вы можете на сайте просматривать информацию доступную пользователям.";
            echo '<a href="../index.php">Exit</a>';
        }
        elseif ($_SESSION['NowLang'] == "ua") {
            echo "Вітаю, Кліент " . $this->name . " " . $this->surname . ". Ви можете на сайті переглядати інформацію доступну користувачам.";
            echo '<a href="../index.php">Exit</a>';
        }
        elseif ($_SESSION['NowLang'] == "it") {
            echo "Salve, cliente " . $this->name . " " . $this->surname . ". È possibile visualizzare le informazioni disponibili per gli utenti sul sito.";
            echo '<a href="../index.php">Exit</a>';
        }
        else {
            echo "Hello, Client " . $this->name . " " . $this->surname . ". You can view information available to users on the site.";
            echo '<a href="../index.php">Exit</a>';
        }
    }
}
?>

