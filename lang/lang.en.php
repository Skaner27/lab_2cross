<?php

$Lang = array(
    "title" => "Lab2",
    "login_lan" => "Login",
    "pass_lan"	=> "Password",
    "button_lan" => "Submit",
    "greeting" => "Welcome!",
    "login" => "Please login",
    "check" => "Check me out",
    "lang" => "Choose language",


    "client"=> "Client",
    "msg1" => "You can view information available to users on the site.",
    "amd" => "Admin",
    "msg2" => "You can do everything on the site.",

);
?>