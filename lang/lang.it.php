<?php

$Lang = array(
    "title" => "Lab2",
    "login_lan" => "Accesso",
    "pass_lan"	=> "Parola d'ordine",
    "button_lan" => "Clic",
    "greeting" => "Benvenuto!",
    "login" => "Accedere prego",
    "check" => "Controllami",
    "lang" => "Scegli la lingua",


    "client"=> "Cliente",
    "msg1" => "È possibile visualizzare le informazioni disponibili per gli utenti sul sito.",
    "amd" => "Admin",
    "msg2" => "Puoi fare tutto sul sito.",

);
?>